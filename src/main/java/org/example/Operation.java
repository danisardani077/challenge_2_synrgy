package org.example;

import java.io.*;
import java.util.*;


public class Operation {
    public Map<String, ModelData> dataMap = new HashMap<>();
    Calculation cal = new Calculation();
    public void readFile(String path){
        try {
            File file = new File(path);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            String[] baris;
            ArrayList<Integer> arrRead = new ArrayList<>();
            int[] arr;

            while ((line = bufferedReader.readLine()) != null) {
                baris = line.split(";");
                ModelData dataRefIn = new ModelData();
                int[] nilai = new int[baris.length - 1];

                for (int i = 1; i < baris.length; i++) {
                    nilai[i -1] = Integer.parseInt(baris[i]);
                    arrRead.add(Integer.parseInt(baris[i]));
                }
                dataRefIn.setKelas(baris[0]);
                dataRefIn.setNilai(nilai);
                dataMap.put(baris[0], dataRefIn);
            }
            ModelData dataRef = new ModelData();
            arr = arrRead.stream().mapToInt(i->i).toArray();
            dataRef.setKelas("SemuaKelas");
            dataRef.setNilai(arr);
            dataMap.put("SemuaKelas", dataRef);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void write2(String pathTarget){
        try {
            File file = new File(pathTarget);
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            for (Map.Entry<String, ModelData> mapSet : dataMap.entrySet()) {
                bw.write(mapSet.getKey());
                bw.write("\nmean : " + String.valueOf(cal.rata2(mapSet.getValue().getNilai())));
                bw.write("\nmedian : " + String.valueOf(cal.nilaiTengah(mapSet.getValue().getNilai())));
                bw.write("\nmodus : " + String.valueOf(cal.modus(mapSet.getValue().getNilai())));
                bw.write("\n\n");
            }
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void write(String pathTarget){
        int[] semuaKelas = dataMap.get("SemuaKelas").getNilai();
        int kurangEnam=0;
        int Enam = 0;
        int tujuh = 0;
        int delapan = 0;
        int sembilan = 0;
        int sepuluh = 0;
        for(int item:semuaKelas){
            if(item<6){
                kurangEnam++;
            } else if (item == 6) {
                Enam++;
            }else if (item == 7) {
                tujuh++;
            }else if (item == 8) {
                delapan++;
            }else if (item == 9) {
                sembilan++;
            }else if (item==10) {
                sepuluh++;
            }
        }
        try {
            File file = new File(pathTarget);
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("Nilai  | Frekuensi\n");
            bw.write("<6");
            bw.write("    |     ");
            bw.write(String.valueOf(kurangEnam));
            bw.write("\n");
            bw.write("6");
            bw.write("    |     ");
            bw.write(String.valueOf(Enam));
            bw.write("\n");
            bw.write("7");
            bw.write("    |     ");
            bw.write(String.valueOf(tujuh));
            bw.write("\n");
            bw.write("8");
            bw.write("    |     ");
            bw.write(String.valueOf(delapan));
            bw.write("\n");
            bw.write("9");
            bw.write("    |     ");
            bw.write(String.valueOf(sembilan));
            bw.write("\n");
            bw.write("10");
            bw.write("    |     ");
            bw.write(String.valueOf(sepuluh));
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
