package org.example;

import java.util.Arrays;

public class Calculation {
    public double rata2(int[] data){
        double total = 0;
        double rata2;
        int panjangData = data.length;
        for (int i = 0; i < panjangData; i++) {
            total += data[i];
        }
        rata2 = total/data.length;
        return rata2;
    }
    public double nilaiTengah(int[] data){
        double nilaiTengah;
        Arrays.sort(data);
        int panjangData = data.length;
        if ((panjangData % 2) == 0){
            nilaiTengah = (data[data.length/2] + data[data.length/2 - 1])/2;
        } else {
            nilaiTengah = data[data.length/2];
        }
        return nilaiTengah;
    }
    public double modus(int[] data){
        int popularity1 = 0;
        int popularity2 = 0;
        int popularity_item = 0;
        int array_item = 0;
        for (int i =0;i<data.length;i++){
            array_item = data[i];
            for (int j =0;j<data.length;j++){
                if(array_item == data[j]) {
                    popularity1++;
                } else if (popularity1 >= popularity2) {
                    popularity_item = array_item;
                    popularity2 = popularity1;
                }
            }
            popularity1 = 0;
        }
        return popularity_item;
    }
}
